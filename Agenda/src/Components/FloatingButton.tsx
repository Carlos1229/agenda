import React from 'react'
import { StyleSheet } from 'react-native'
import { FAB } from 'react-native-paper'
import { useTheme } from '@/Theme'

interface PropsFAB {
  icon: string
}

const FloatingButton = ({ icon }: PropsFAB) => {
  const { Common } = useTheme()
  return (
    <FAB
      style={[styles.fab, Common.backgroundPrimary]}
      icon={icon}
      onPress={() => console.log('Pressed')}
      color="white"
    />
  )
}
const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
})

export default FloatingButton
