import React from 'react'
import { View, Text } from 'react-native'
import { Avatar } from 'react-native-paper'
import { useTheme } from '@/Theme'

const ImageError = () => {
  const { Layout, Fonts, Common, Gutters } = useTheme()

  return (
    <View style={[Layout.fill, Layout.center]}>
      <Avatar.Icon
        size={150}
        icon="emoticon-sad-outline"
        style={[Common.backgroundError, Gutters.regularBMargin]}
        color="white"
      />
      <Text style={Fonts.textRegularBold}>Lo sentimos, error inesperado</Text>
    </View>
  )
}

export default ImageError
