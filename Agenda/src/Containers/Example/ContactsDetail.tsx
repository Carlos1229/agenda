import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  ActivityIndicator,
  StyleSheet,
  ScrollView,
} from 'react-native'
import { Avatar, List, FAB, IconButton } from 'react-native-paper'
import { useTheme } from '@/Theme'
import api from '@/Services'
import { ImageError, FloatingButton } from '@/Components/'
import { doCall, doSms } from '@/Services/utils/doActions'

const ContactsDetail = (props: any) => {
  const { Common, Fonts, Gutters, Layout, Colors } = useTheme()

  //obj: SE GUARDA EL OBJETO QUE NOS REGRESA LA CONSULTA
  const [objUser, setObjUser] = useState({})
  //boolean: SI HAY ERROR EN CONSULTA SE CAMBIARA A "true"
  const [error, setError] = useState(false)

  //SE CAPTURA LOS PARAMETROS QUE SE MANDAN DESDE LA VISTA "ConactsList"
  const { params } = props.route
  let id = params.idUser

  useEffect(() => {
    consulta()
  }, [])

  /*SE REALIZA UNA CONSULTA DESDE LA API
    .get(""): "users/<id>" le mandamos este parametro para la consulta con su respectivo id
    .then(): capturamos la respuesta que nos manda el servidor y lo guardamos en el "arrayUsers"
    .catch(): captura algun error que haya pasado, y el state "error" pasa a "true"*/
  const consulta = async () => {
    await api
      .get(`users/${id}`)
      .then(response => setObjUser(response.data))
      .catch(err => setError(true))
  }
  const listCallHistory = [
    {
      type: 'Incoming',
      phone: '417-799-0717',
      hour: '3:45pm',
    },
    {
      type: 'Outgoing',
      phone: '417-799-0717',
      hour: '10:00pm',
    },
    {
      type: 'Incoming',
      phone: '417-799-0717',
      hour: '11:40pm',
    },
  ]
  return (
    <View style={Layout.fill}>
      {error ? (
        <ImageError />
      ) : (
        <>
          <FloatingButton icon="square-edit-outline" />
          <ScrollView
            contentContainerStyle={[
              [
                Layout.alignItemsCenter,
                Layout.scrollSpaceAround,
                Gutters.smallVMargin,
                Gutters.largeHPadding,
              ],
            ]}
          >
            {Object.keys(objUser).length === 0 ? (
              <ActivityIndicator color={Colors.primary} size="large" />
            ) : (
              <>
                <Avatar.Image
                  size={180}
                  source={{
                    uri: `https://randomuser.me/api/portraits/men/${id}.jpg`,
                  }}
                  style={[Common.backgroundPrimary, Gutters.regularBMargin]}
                />
                <View style={[Gutters.regularBMargin, Layout.alignItemsCenter]}>
                  <Text
                    style={[Fonts.titleSmall]}
                    numberOfLines={1}
                    adjustsFontSizeToFit
                  >
                    {objUser.name}
                  </Text>
                  <Text style={Fonts.textRegular}>{objUser.phone}</Text>
                </View>
                {/* VIEW ICONS */}
                <View
                  style={[
                    Layout.fullWidth,
                    Layout.justifyContentAround,
                    Layout.row,
                  ]}
                >
                  <IconButton
                    size={33}
                    icon="video"
                    color="white"
                    style={Common.backgroundPrimary}
                  />
                  <IconButton
                    size={33}
                    icon="phone"
                    color="white"
                    style={Common.backgroundPrimary}
                    onPress={() => doCall(objUser.phone)}
                  />
                  <IconButton
                    size={33}
                    icon="message"
                    color="white"
                    style={Common.backgroundPrimary}
                    onPress={() => doSms(objUser.phone)}
                  />
                </View>
                <>
                  {/* TITLE VIEW TO CALLS */}
                  <View
                    style={[
                      Layout.fullWidth,
                      Layout.justifyContentBetween,
                      Layout.row,
                    ]}
                  >
                    <Text style={Fonts.textRegularBold}>Call History</Text>
                    <Text style={Fonts.textUltraSmall}>View All</Text>
                  </View>
                  {/* VIEW LIST CALLS */}
                  <View style={Layout.fullWidth}>
                    {listCallHistory.map((item, index) => (
                      <List.Item
                        key={index}
                        titleStyle={Fonts.textSmallBold}
                        style={styles.itemPadding}
                        title={item.type}
                        description={item.phone}
                        right={props => (
                          <Text style={Layout.selfCenter}>{item.hour}</Text>
                        )}
                      />
                    ))}
                  </View>
                </>
              </>
            )}
          </ScrollView>
        </>
      )}
    </View>
  )
}
const styles = StyleSheet.create({
  itemPadding: {
    paddingRight: 0,
    paddingLeft: 0,
  },
})
export default ContactsDetail
