import React, { useEffect, useState } from 'react'
import { View, ScrollView, ActivityIndicator } from 'react-native'
import { List, Avatar, IconButton } from 'react-native-paper'
import { useTheme } from '@/Theme'
import api from '@/Services'
import { FloatingButton, ImageError } from '@/Components/'
import { doCall, doSms } from '@/Services/utils/doActions'
import { InterfaceUsers } from '@/Services/interfaces/interfaceUsers'

const ContactsList = ({ navigator }: pros) => {
  const { Colors, Fonts, Common, Layout } = useTheme()

  //array: GUARDA LOS USUARIOS QUE SE CONSULTARON
  const [arrayUsers, setArrayUsers] = useState([])
  //boolean: CAMBIA A "true", SI HUBO ALGUN ERROR EN LA CONSULTA
  const [error, setError] = useState(false)

  useEffect(() => {
    consulta()
  }, [])

  /*SE REALIZA UNA CONSULTA DESDE LA API
    .get(""): "users/" le mandamos este parametro para la consulta
    .then(): capturamos la respuesta que nos manda el servidor y lo guardamos en el "arrayUsers"
    .catch(): captura algun error que haya pasado, y el state "error" pasa a "true"*/
  const consulta = async () => {
    await api
      .get(`users/`)
      .then(response => setArrayUsers(response.data))
      .catch(err => setError(true))
  }

  return (
    <View style={Layout.fill}>
      {/* SI "error" es "true", HUBO ALGUN ERROR EN LA CONSULTA Y SE MOSTRARA 
            EL VIEW DE "ImageError" */}
      {error === true ? (
        <ImageError />
      ) : (
        <>
          {/* BOTON FLOTANTE ESQUINA INFERIOR DERECHA */}
          <FloatingButton icon="magnify" />
          <ScrollView>
            {/* VALIDA SI YA HAY REGISTROS EN EL STATE "arrayUsers" */}
            {arrayUsers.length === 0 ? (
              <ActivityIndicator color={Colors.primary} size="large" />
            ) : (
              // TRAEMOS LOS DATOS PARA REALIZAR LA VISTA DE UNA LISTA
              arrayUsers.map((item: InterfaceUsers, index) => (
                <List.Item
                  key={index}
                  title={item.name}
                  titleStyle={Fonts.textSmallBold}
                  description={item.phone}
                  left={props => (
                    <Avatar.Image
                      size={50}
                      source={{
                        uri: `https://randomuser.me/api/portraits/med/men/${item.id}.jpg`,
                      }}
                      style={[Common.backgroundPrimary, Layout.selfCenter]}
                    />
                  )}
                  right={props => (
                    <View style={Layout.row}>
                      <IconButton
                        icon="phone"
                        size={25}
                        color={Colors.primary}
                        onPress={() => doCall(item.phone)}
                      />
                      <IconButton
                        icon="message"
                        size={25}
                        color={Colors.primary}
                        onPress={() => doSms(item.phone)}
                      />
                      <IconButton
                        icon="dots-vertical"
                        size={25}
                        color={Colors.grayIcon}
                        onPress={() =>
                          navigator.navigate('Details', { idUser: item.id })
                        }
                      />
                    </View>
                  )}
                  onPress={() =>
                    navigator.navigate('Details', { idUser: item.id })
                  }
                />
              ))
            )}
          </ScrollView>
        </>
      )}
    </View>
  )
}

export default ContactsList
