import React from 'react'
import { View } from 'react-native'
import { useTheme } from '@/Theme'
import { useTranslation } from 'react-i18next'
import ContactsList from './ContactsList'
import { useNavigation } from '@react-navigation/native'

const IndexExampleContainer = () => {
  const { Layout } = useTheme()
  const navigator = useNavigation()
  return (
    <View style={[Layout.fill]}>
      <ContactsList navigator={navigator} />
    </View>
  )
}

export default IndexExampleContainer
