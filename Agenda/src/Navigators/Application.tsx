import React, { useEffect, useState, FunctionComponent } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import {
  IndexStartupContainer,
  IndexExampleContainer,
  ContactsDetail,
} from '@/Containers'
import { useSelector } from 'react-redux'
import { NavigationContainer } from '@react-navigation/native'
import { navigationRef } from '@/Navigators/Root'
import { SafeAreaView, StatusBar } from 'react-native'
import { useTheme } from '@/Theme'
import { StartupState } from '@/Store/Startup'

const Stack = createStackNavigator()

// @refresh reset
const ApplicationNavigator = () => {
  const { Layout, darkMode, NavigationTheme, Fonts } = useTheme()
  const { colors } = NavigationTheme
  const [isApplicationLoaded, setIsApplicationLoaded] = useState(false)
  const applicationIsLoading = useSelector(
    (state: { startup: StartupState }) => state.startup.loading,
  )

  useEffect(() => {
    setIsApplicationLoaded(true)
  }, [applicationIsLoading])

  // on destroy needed to be able to reset when app close in background (Android)
  useEffect(
    () => () => {
      setIsApplicationLoaded(false)
    },
    [],
  )

  return (
    <SafeAreaView style={[Layout.fill, { backgroundColor: colors.card }]}>
      <NavigationContainer theme={NavigationTheme} ref={navigationRef}>
        <StatusBar barStyle={darkMode ? 'light-content' : 'dark-content'} />
        <Stack.Navigator
          screenOptions={{
            // headerShown: false,
            headerStyle: {
              elevation: 0, //para ANDROID
              shadowColor: 'transparent', //PARA IOS
            },
            cardStyle: {
              backgroundColor: 'white',
            },
          }}
        >
          <Stack.Screen
            name="Startup"
            component={IndexStartupContainer}
            options={{ headerShown: false }}
          />
          {isApplicationLoaded != null && (
            <>
              <Stack.Screen
                name="Main"
                component={IndexExampleContainer}
                options={{
                  animationEnabled: true,
                  title: 'Contact List',
                  headerTitleStyle: Fonts.textRegularBold,
                  headerTitleAlign: 'center',
                }}
              />
              <Stack.Screen
                name="Details"
                component={ContactsDetail}
                options={{
                  animationEnabled: true,
                  title: 'Profile',
                  headerTitleStyle: Fonts.textRegularBold,
                  headerTitleAlign: 'center',
                }}
              />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  )
}

export default ApplicationNavigator
