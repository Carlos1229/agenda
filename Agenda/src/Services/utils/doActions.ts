import React from "react"
import {Platform,Linking} from "react-native"

/*  METODOS QUE REALIZAN ACCIONES:
    doCall: PARA ABRIR APP TELEFONO Y LLAMAR
    doSms: ABRE LA APP DE MENSAJES
    AMBOS RECIBEN EL PARAMETRO "number"*/

export const doCall = (number:string) => {
    let phoneNumber = ''
    if (Platform.OS === 'android') {
      phoneNumber = `tel:${number}`
    } else {
      phoneNumber = `telprompt:${number}`
    }
    Linking.openURL(phoneNumber)
  }

 export const doSms = (number:string) => {
    let phoneNumber = ''
    if (Platform.OS === 'android') {
      phoneNumber = `sms:${number}`
    } else {
      phoneNumber = `telprompt:${number}`
    }
    Linking.openURL(phoneNumber)
  }